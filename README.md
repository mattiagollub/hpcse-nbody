# N-Body Simulation #

Developed as part of the High Performance Computing for CSE class, the project demonstrates an efficient simulation of the motion of an N-body system under the effect of Lennard-Jones forces. The software is written in C++, uses CUDA for GPU acceleration and is configured to run on ETH's cluster Brutus. 