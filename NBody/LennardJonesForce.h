//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "System.h"
#include "Force.h"

//==============================================================================//
//		Evaluation of the Lennard-Jones force.									//
//==============================================================================//

class LennardJonesForce : public Force
{
public:

	//==========================================================================//
	//	Constructor.															//
	//==========================================================================//

	LennardJonesForce(real sigma, real epsilon);

	//==========================================================================//
	//	Evaluation.																//
	//==========================================================================//

	Vector3 Evaluate(const Vector3& ri, const Vector3& rj, const System* system);
	real EvaluatePotential(const Vector3& ri, const Vector3& rj, const System* system);

	//==========================================================================//
	//	CUDA implementation.													//
	//==========================================================================//

	#ifdef CCUDA
	cudaFuncs SetupCUDA();
	#endif	
	
private:
	real s;
	real e;
};
