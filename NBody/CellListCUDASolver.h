//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Solver.h"

#include <thrust/device_vector.h>

//==============================================================================//
//		Cell list CUDA NBody solver.											//
//==============================================================================//

class CellListCUDASolver : public Solver
{
public:

	//==========================================================================//
	//	Constructor / Destructor.												//
	//==========================================================================//
	
	CellListCUDASolver(size_t cells);
	~CellListCUDASolver();

	//==========================================================================//
	//	Solver-specific methods.												//
	//==========================================================================//

	void Synchronize();

protected:
	real OnEvaluatePotential();
	void OnAdvance();
	void OnSetStatus(System* system);

private:

	//==========================================================================//
	//	Private methods.														//
	//==========================================================================//

	Vector3 EvaluateForce(const size_t& j);
	void CreateStructures();
	void DeleteBuffers();
	void CreateBuffers();
	size_t ComputeCellId(size_t particleId);
	void SortArrays();

	//==========================================================================//
	//	Private fields.															//
	//==========================================================================//

	real h;
	size_t M;
	thrust::device_vector<CellParticle> devSB;
	ParticleInterval* devSS;

	float3* devR;
	float3* devV;
	float3* devA;
	float3* devAux;
	thrust::device_vector<float> devPot;
};	
