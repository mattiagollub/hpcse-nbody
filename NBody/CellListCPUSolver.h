//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Solver.h"

//==============================================================================//
//		Cell list CPU NBody solver.												//
//==============================================================================//

class CellListCPUSolver : public Solver
{
public:

	//==========================================================================//
	//	Constructor / Destructor.												//
	//==========================================================================//
	
	CellListCPUSolver(size_t cells);
	~CellListCPUSolver();
	
	//==========================================================================//
	//	Solver-specific methods.												//
	//==========================================================================//

	void Synchronize();

protected:
	real OnEvaluatePotential();
	void OnAdvance();
	void OnSetStatus(System* system);

private:

	//==========================================================================//
	//	Private methods.														//
	//==========================================================================//

	Vector3 EvaluateForce(const size_t& j);
	void CreateStructures();
	void DeleteBuffers();
	void CreateBuffers();
	size_t ComputeCellId(size_t particleId);
	void SortArrays();

	//==========================================================================//
	//	Private fields.															//
	//==========================================================================//

	real h;
	size_t M;
	CellParticle* A;
	CellParticle* B;
	ParticleInterval* S;
};
