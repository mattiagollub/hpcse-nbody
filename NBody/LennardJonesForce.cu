//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "LennardJonesForce.h"
#include "LennardJonesForce.cuh"
#include "cuTypes.cuh"

#include <cuda_runtime.h>

//==============================================================================//
//		CUDA implementation.													//
//==============================================================================//

__device__ __constant__ cuLJF LJFdata;

__device__ float3 LJF(const float3& ri, const float3& rj)
{
	float3 r = rj - ri;
	float nr = sqrt(r * r);

	if (nr > LJFdata.cutoff)
		return make_float3(0.0f, 0.0f, 0.0f);

	return -24.0f * LJFdata.e * (2.0f * pow(LJFdata.s / nr, 12.0f) - pow(LJFdata.s / nr, 6.0f)) * r / (nr * nr);
}

__device__ float LJP(const float3& ri, const float3& rj)
{
	float3 r = ri - rj;
	float d = sqrt(r * r);
	float pot = 4.0f * LJFdata.e * (pow(LJFdata.s / d, 12.0f) - pow(LJFdata.s / d, 6.0f));

	if (d < LJFdata.cutoff)
		pot -= LJFdata.Urcut;

	return pot;
}

__device__ __constant__ forceFunc LJFfunc = &LJF;
__device__ __constant__ potentialFunc LJPfunc = &LJP;

cudaFuncs LennardJonesForce::SetupCUDA()
{
	cuLJF cuf;
	cuf.cutoff = (float)cutoff;
	cuf.e = (float)e;
	cuf.s = (float)s;
	cuf.Urcut = Urcut;

	cudaFuncs f;

	CC(cudaMemcpyToSymbol(LJFdata, &cuf, sizeof(cuLJF)));
	CC(cudaMemcpyFromSymbol(&f.f, LJFfunc, sizeof(forceFunc)));
	CC(cudaMemcpyFromSymbol(&f.p, LJPfunc, sizeof(potentialFunc)));

	return f;
}
