//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Commons.h"
#include "LennardJonesForce.h"

#include <cmath>

#ifdef CCUDA
#include "LennardJonesForce.cuh"
#endif

using namespace std;

//==============================================================================//
//		Evaluation of the Lennard-Jones force.									//
//==============================================================================//

LennardJonesForce::LennardJonesForce(real sigma, real epsilon)
{
	s = sigma;
	e = epsilon;
}

Vector3 LennardJonesForce::Evaluate(const Vector3& ri, const Vector3& rj, const System* system)
{
	Vector3 r = rj - ri;
	real nr = r.Norm();

	if (nr > cutoff)
		return Vector3();

	return -(real)24.0 * e * ((real)2.0 * powr(s / nr, (real)12.0) - powr(s / nr, (real)6.0)) * r / (nr * nr);
}

real LennardJonesForce::EvaluatePotential(const Vector3& ri, const Vector3& rj, const System* system)
{
	real d = (ri - rj).Norm();
	real pot = (real)4.0 * e * (powr(s / d, (real)12.0) - powr(s / d, (real)6.0));

	if (d < cutoff)
		pot -= Urcut;

	return pot;
}
