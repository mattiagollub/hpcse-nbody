//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Solver.h"

#include <cuda_runtime.h>
#include <thrust/device_vector.h>

//==============================================================================//
//		Simple CUDA NBody solver.												//
//==============================================================================//

class SimpleCUDASolver : public Solver
{
public:

	//==========================================================================//
	//	Constructor.															//
	//==========================================================================//
	
	SimpleCUDASolver();
	~SimpleCUDASolver();

	//==========================================================================//
	//	Solver-specific methods.												//
	//==========================================================================//

	void Synchronize();

protected:
	real OnEvaluatePotential();
	void OnAdvance();
	void OnSetStatus(System* system);

private:

	//==========================================================================//
	//	Private methods.														//
	//==========================================================================//

	void CreateBuffers();
	void DeleteBuffers();

	//==========================================================================//
	//	Private fields.															//
	//==========================================================================//

	float3*	devR;
	float3*	devV;
	float3* devA;

	thrust::device_vector<float> devAux;
};