//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include <cuda_runtime.h>

//==============================================================================//
//		float3 operators for CUDA.												//
//																				//
//		The following code may contain useless functions, it is adapted			//
//		from an old personal project.											//
//==============================================================================//

__device__ float3 operator- (const float3& v)
{
	return make_float3(-v.x, -v.y, -v.z);
}
	
__device__ float3 operator+ (const float3& v1, const float3& v2)
{
	return make_float3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
}

__device__ float3 operator- (const float3& v1, const float3& v2)
{
	return make_float3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
}

__device__ float operator* (const float3& v1, const float3& v2)
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

__device__ float3 operator+= (float3& v1, const float3& v2)
{
	v1.x += v2.x;
	v1.y += v2.y;
	v1.z += v2.z;

	return v1;
}

__device__ float3 operator-= (float3& v1, const float3& v2)
{
	v1.x -= v2.x;
	v1.y -= v2.y;
	v1.z -= v2.z;

	return v1;
}
		
__device__ float3 operator*(const float3& v, const float& t)
{
	return make_float3(v.x * t, v.y * t, v.z * t);
}

__device__ float3 operator*(const float& t, const float3& v)
{
	return make_float3(v.x * t, v.y * t, v.z * t);
}

__device__ float3 operator/ (const float3& v, const float c)
{
	return make_float3(v.x / c, v.y / c, v.z / c);
}