//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Commons.h"

//==============================================================================//
//		3-components vector.													//
//																				//
//		The following code may contain useless functions, it is adapted			//
//		from an old personal project.											//
//==============================================================================//

class Vector3
{
public:

	// Vector components
	//====================
	real x;
	real y;
	real z;

	// Constructors
	//===============
	Vector3();
	Vector3(real x, real y, real z);

	// Operators
	//============
	Vector3 operator= (const Vector3& v);			// Assignment
	Vector3 operator- () const;						// Negation
	
	Vector3 operator+ (const Vector3& v) const;		// Vector addition
	Vector3 operator- (const Vector3& v) const;		// Vector subtraction
	real operator* (const Vector3& v) const;		// Scalar product
	Vector3 operator^ (const Vector3& v) const;		// Vectorial product
	Vector3 operator| (Vector3& v);					// Projection
	
	Vector3 operator+= (const Vector3& v);			// Vector addition
	Vector3 operator-= (const Vector3& v);			// Vector subtraction
	
	Vector3 operator* (const real c) const;			// Scalar multiplication
	Vector3 operator/ (const real c) const;			// Scalar division
	
	Vector3 operator*= (const real c);				// Scalar multiplication
	Vector3 operator/= (const real c);				// Scalar division

	bool operator== (const Vector3& v) const;		// Vector equality
	bool operator!= (const Vector3& v) const;

	// Vector methods
	//=================
	real	Norm();
	real	NormSquared();
	void	Normalize();
	real	AngleBetween(Vector3& v);
	bool	IsZero() const;
	Vector3 Sign();
};

//==============================================================================//
//		external operators														//
//==============================================================================//
	
Vector3 operator* (real const& t, Vector3 const& v);

//==============================================================================//
//		Vector3 class implementation.											//
//		(The inline keyword requires the implementation to be in the header)	//
//==============================================================================//

inline Vector3::Vector3()
{
	this->x = (real)0.0;
	this->y = (real)0.0;
	this->z = (real)0.0;
}

inline Vector3::Vector3(real x, real y, real z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

inline Vector3 Vector3::operator= (const Vector3& v)
{
	if (this == &v)
		return (*this);
		
	x = v.x;
	y = v.y;
	z = v.z;

	return (*this);
}

inline Vector3 Vector3::operator- () const
{
	return Vector3(-x, -y, -z);
}
	
inline Vector3 Vector3::operator+ (const Vector3& v) const
{
	return Vector3(x + v.x, y + v.y, z + v.z);
}

inline Vector3 Vector3::operator- (const Vector3& v) const
{
	return Vector3(x - v.x, y - v.y, z - v.z);
}

inline real Vector3::operator* (const Vector3& v) const
{
	return x * v.x + y * v.y + z * v.z;
}

inline Vector3 Vector3::operator^ (const Vector3& v) const
{
	return Vector3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
}

inline Vector3 Vector3::operator| (Vector3& v)
{
	real scalarProd = (*this) * v;
	real vNorm = v.Norm();
	real projectionCoefficient = scalarProd / (vNorm * vNorm);
	return Vector3(v.x * projectionCoefficient, v.y * projectionCoefficient, v.z * projectionCoefficient);
}

inline Vector3 Vector3::operator+= (const Vector3& v)
{
	x += v.x;
	y += v.y;
	z += v.z;

	return (*this);
}

inline Vector3 Vector3::operator-= (const Vector3& v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;

	return (*this);
}
		
inline Vector3 Vector3::operator* (const real c) const
{
	return Vector3(x * c, y * c, z * c);
}

inline Vector3 Vector3::operator/ (const real c) const
{
	return Vector3(x / c, y / c, z / c);
}

inline Vector3 Vector3::operator*= (const real c)
{
	x *= c;
	y *= c;
	z *= c;

	return *this;
}

inline Vector3 Vector3::operator/= (const real c)
{
	x /= c;
	y /= c;
	z /= c;

	return *this;
}

inline bool Vector3::operator== (const Vector3& v) const
{
	return x == v.x && y == v.y && z == v.z;
}

inline bool Vector3::operator!= (const Vector3& v) const
{
	return x != v.x && y != v.y && z != v.z;
}

inline Vector3 operator*(real const& t, Vector3 const& v)
{
	return Vector3(v.x * t, v.y * t, v.z * t);
}

inline real Vector3::Norm()
{
	return sqrtr(x * x + y * y + z * z);
}

inline real Vector3::NormSquared()
{
	return x * x + y * y + z * z;
}

inline void Vector3::Normalize()
{
	real norm = sqrtr(x * x + y * y + z * z); 

	x /= norm;
	y /= norm;
	z /= norm;
}

inline real Vector3::AngleBetween(Vector3& v)
{
	real scalarProd = (*this) * v;
	real thisNorm = this->Norm();
	real vNorm = v.Norm();

	return acos(scalarProd / (thisNorm * vNorm));
}

inline bool Vector3::IsZero() const
{
	return (x == (real)0.0) & (y == (real)0.0) & (z == (real)0.0);
}

inline Vector3 Vector3::Sign()
{
	return Vector3(
		(x >= (real)0.0) ? (real)1.0 : (real)-1.0,
		(y >= (real)0.0) ? (real)1.0 : (real)-1.0,
		(z >= (real)0.0) ? (real)1.0 : (real)-1.0);
}
