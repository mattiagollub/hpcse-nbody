/*
 *  Timer.h
 *
 *	Timer class using gettimeofdays
 *
 *  Created by Christian Conti on 10/1/13.
 *  Copyright 2013 ETH Zurich. All rights reserved.
 *
 *  Windows implementation by Mattia Gollub
 */

#pragma once
#ifdef WIN32
#include <Windows.h>
#else
#include <sys/time.h>
#endif

class Timer
{
#ifdef WIN32
	double ticksPerMs;
	signed long long startTick;
#else
	struct timeval t_start, t_end;
	struct timezone t_zone;
#endif

public:
	#ifdef WIN32
	Timer()
	{
		INT64 frequency;
		QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);
	
		ticksPerMs = (double)(frequency / 1000);
	}
	#endif

	void start()
	{
		#ifdef WIN32
		QueryPerformanceCounter((LARGE_INTEGER *)&startTick);
		#else
		gettimeofday(&t_start,  &t_zone);
		#endif
	}
	
	double stop()
	{
		#ifdef WIN32
		INT64 stopTick;
		QueryPerformanceCounter((LARGE_INTEGER *)&stopTick);
	
		return (double)(stopTick - startTick) / ticksPerMs / 1000.0f;
		#else
		gettimeofday(&t_end,  &t_zone);
		return (t_end.tv_usec  - t_start.tv_usec)*1e-6  + (t_end.tv_sec  - t_start.tv_sec);
		#endif
	}
};
