//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Commons.h"
#include "Force.h"

class System;

//==============================================================================//
//		Base class fo any NBody solver.											//
//==============================================================================//

class Solver
{
public:

	//==========================================================================//
	//	Constructor / Initialization.											//
	//==========================================================================//
	
	Solver();

	//==========================================================================//
	//	Solver methods.															//
	//==========================================================================//

	void SetTimestep(real dt);
	void SetStatus(System* system);

	void AdvanceOneStep(bool computePotential);
	real PotentialEnergy();
	virtual void Synchronize() = 0;

protected:
	virtual real OnEvaluatePotential() = 0;
	virtual void OnAdvance() = 0;
	virtual void OnSetStatus(System* system) = 0;

	//==========================================================================//
	//	Protected fields.														//
	//==========================================================================//

	real dt;
	real epot;
	System* system;
};