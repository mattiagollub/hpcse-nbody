//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "SimpleCPUSolver.h"
#include "System.h"

#include <cmath>

using namespace std;

//==============================================================================//
//		SimpleCPUSolver class implementation.									//
//==============================================================================//

SimpleCPUSolver::SimpleCPUSolver()
{
}

Vector3 SimpleCPUSolver::EvaluateForce(const size_t& j)
{
	Vector3 force = Vector3();

	for (size_t i = 0; i < system->N; i++)
	{
		if (i != j)
		{
			Vector3 ri = system->r[i];
			Vector3 rj = system->r[j];
			Vector3 dist =  ri - rj;
			Vector3 sign = dist.Sign();

			// From the definition of periodic boundary conditions on wikipedia
			if (abs(dist.x) > (real)1.0) ri.x -= (real)2.0 * sign.x;
			if (abs(dist.y) > (real)1.0) ri.y -= (real)2.0 * sign.y;
			if (abs(dist.z) > (real)1.0) ri.z -= (real)2.0 * sign.z;

			force += system->force->Evaluate(rj, ri, system);
		}
	}

	return force;
}

void SimpleCPUSolver::OnAdvance()
{
	real dtover2 = dt / (real)2.0;

	// K2
	//=====
	for (size_t i = 0; i < system->N; i++)
	{
		system->v[i] = system->v[i] + dtover2 * system->a[i];
	}
	
	// K1
	//=====
	for (size_t i = 0; i < system->N; i++)
	{
		system->r[i] = system->r[i] + dt * system->v[i];

		// Periodic boundary conditions
		//===============================
		// Incorrect if we allow movements of more than 2.0 in one step, 
		// but in that case the simulation would bed completely unstable anyway.
		if (system->r[i].x < (real)-1.0) system->r[i].x += (real)2.0;
		if (system->r[i].x >  (real)1.0) system->r[i].x -= (real)2.0;
		
		if (system->r[i].y < (real)-1.0) system->r[i].y += (real)2.0;
		if (system->r[i].y >  (real)1.0) system->r[i].y -= (real)2.0;
		
		if (system->r[i].z < (real)-1.0) system->r[i].z += (real)2.0;
		if (system->r[i].z >  (real)1.0) system->r[i].z -= (real)2.0;		
	}
	
	// Update accelerations
	//=======================
	for (size_t i = 0; i < system->N; i++)
	{
		system->a[i] = EvaluateForce(i) / system->m;
	}
	
	// K2
	//=====
	for (size_t i = 0; i < system->N; i++)
	{
		system->v[i] = system->v[i] + dtover2 * system->a[i];
	}
}

void SimpleCPUSolver::OnSetStatus(System* system)
{
	real dtover2 = dt / (real)2.0;

	// Compute accelerations for first step
	//=======================================
	for (size_t i = 0; i < system->N; i++)
	{
		system->a[i] = EvaluateForce(i) / system->m;
	}
}

void SimpleCPUSolver::Synchronize()
{
	// Not necessary for this solver
}

real SimpleCPUSolver::OnEvaluatePotential()
{
	real pot = (real)0.0f;

	for (size_t i = 0; i < system->N; i++)
	{
		for (size_t j = 0; j < i; j++)
		{
			Vector3 ri = system->r[i];
			Vector3 rj = system->r[j];
			Vector3 dist =  ri - rj;
			Vector3 sign = dist.Sign();

			// From the definition of periodic boundary conditions on wikipedia
			if (abs(dist.x) > (real)1.0) ri.x -= (real)2.0 * sign.x;
			if (abs(dist.y) > (real)1.0) ri.y -= (real)2.0 * sign.y;
			if (abs(dist.z) > (real)1.0) ri.z -= (real)2.0 * sign.z;

			pot += system->force->EvaluatePotential(rj, ri, system);
		}
	}

	return pot;
}