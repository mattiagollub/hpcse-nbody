//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "CellListCPUSolver.h"
#include "System.h"

#include <string.h>
#include <cmath>

using namespace std;

//==============================================================================//
//		CellListCPUSolver class implementation.									//
//==============================================================================//

CellListCPUSolver::CellListCPUSolver(size_t cells)
{
	h = (real)2.0 / (real)cells;
	M = cells;
	
	A = NULL;
	B = NULL;
	S = NULL;
}

CellListCPUSolver::~CellListCPUSolver()
{
	DeleteBuffers();
}

real CellListCPUSolver::OnEvaluatePotential()
{
	real pot = 0;
	
	for (size_t j = 0; j  < system->N; j++)
	{
		// Find adjacent cells
		//======================
		size_t cid = B[j].cellId;
		size_t x, y, z;
		size_t xm, xp, ym, yp, zm, zp;

		x = cid % M;
		y = (cid % (M*M)) / M;
		z = cid / (M*M);

		if (x == 0) xm = M - 1;	else xm = x - 1;
		if (x == (M - 1)) xp = 0; else xp = x + 1;

		if (y == 0) ym = M - 1; else ym = y - 1;
		if (y == (M - 1)) yp = 0; else yp = y + 1;

		if (z == 0) zm = M - 1;	else zm = z - 1;
		if (z == (M - 1)) zp = 0; else zp = z + 1;

		size_t cx[27] = { xm, xm, xm, xm, xm, xm, xm, xm, xm,  x,  x,  x,  x,  x,  x,  x,  x,  x, xp, xp, xp, xp, xp, xp, xp, xp, xp}; 
		size_t cy[27] = { ym, ym, ym,  y,  y,  y, yp, yp, yp, ym, ym, ym,  y,  y,  y, yp, yp, yp, ym, ym, ym,  y,  y,  y, yp, yp, yp};
		size_t cz[27] = { zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp};
		
		// Compute potentials
		//=====================
		for (size_t c = 0; c < 27; c++)
		{
			cid = cx[c] + cy[c] * M + cz[c] * M * M;
			
			for (size_t i = S[cid].start; i < S[cid].end; i++)
			{
				if (j < i)
				{
					Vector3 ri = system->r[i];
					Vector3 rj = system->r[j];
					Vector3 dist =  ri - rj;
					Vector3 sign = dist.Sign();

					// From the definition of periodic boundary conditions on wikipedia
					if (abs(dist.x) > 1.0f) ri.x -= (real)2.0 * sign.x;
					if (abs(dist.y) > 1.0f) ri.y -= (real)2.0 * sign.y;
					if (abs(dist.z) > 1.0f) ri.z -= (real)2.0 * sign.z;
		
					pot += system->force->EvaluatePotential(rj, ri, system);
				}
			}
		}
	}

	return pot;
}

Vector3 CellListCPUSolver::EvaluateForce(const size_t& j)
{
	Vector3 force = Vector3();

	// Find adjacent cells
	//======================
	size_t cid = B[j].cellId;
	size_t x, y, z;
	size_t xm, xp, ym, yp, zm, zp;

	x = cid % M;
	y = (cid % (M*M)) / M;
	z = cid / (M*M);

	if (x == 0) xm = M - 1;	else xm = x - 1;
	if (x == (M - 1)) xp = 0; else xp = x + 1;

	if (y == 0) ym = M - 1; else ym = y - 1;
	if (y == (M - 1)) yp = 0; else yp = y + 1;

	if (z == 0) zm = M - 1;	else zm = z - 1;
	if (z == (M - 1)) zp = 0; else zp = z + 1;

	size_t cx[27] = { xm, xm, xm, xm, xm, xm, xm, xm, xm,  x,  x,  x,  x,  x,  x,  x,  x,  x, xp, xp, xp, xp, xp, xp, xp, xp, xp}; 
	size_t cy[27] = { ym, ym, ym,  y,  y,  y, yp, yp, yp, ym, ym, ym,  y,  y,  y, yp, yp, yp, ym, ym, ym,  y,  y,  y, yp, yp, yp};
	size_t cz[27] = { zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp};
	
	// Compute forces
	//=================
	for (size_t c = 0; c < 27; c++)
	{
		cid = cx[c] + cy[c] * M + cz[c] * M * M;
		
		for (size_t i = S[cid].start; i < S[cid].end; i++)
		{
			if (i != j)
			{
				Vector3 ri = system->r[i];
				Vector3 rj = system->r[j];
				Vector3 dist =  ri - rj;
				Vector3 sign = dist.Sign();

				// From the definition of periodic boundary conditions on wikipedia
				if (abs(dist.x) > 1.0f) ri.x -= (real)2.0 * sign.x;
				if (abs(dist.y) > 1.0f) ri.y -= (real)2.0 * sign.y;
				if (abs(dist.z) > 1.0f) ri.z -= (real)2.0 * sign.z;
	
				force += system->force->Evaluate(rj, ri, system);
			}
		}
	}

	return force;
}

void CellListCPUSolver::OnAdvance()
{
	real dtover2 = dt / (real)2.0;

	// K2
	//=====
	for (size_t i = 0; i < system->N; i++)
	{
		system->v[i] = system->v[i] + dtover2 * system->a[i];
	}
	
	// K1
	//=====
	for (size_t i = 0; i < system->N; i++)
	{
		system->r[i] = system->r[i] + dt * system->v[i];

		// Periodic boundary conditions
		//===============================
		// Incorrect if we allow movements of more than 2.0 in one step, 
		// but in that case the simulation would bed completely unstable anyway.
		if (system->r[i].x < (real)-1.0) system->r[i].x += (real)2.0;
		if (system->r[i].x >  (real)1.0) system->r[i].x -= (real)2.0;
		
		if (system->r[i].y < (real)-1.0) system->r[i].y += (real)2.0;
		if (system->r[i].y >  (real)1.0) system->r[i].y -= (real)2.0;
		
		if (system->r[i].z < (real)-1.0) system->r[i].z += (real)2.0;
		if (system->r[i].z >  (real)1.0) system->r[i].z -= (real)2.0;		
	}
	
	// Recreate support structures after moving the particles
	//=========================================================
	CreateStructures();

	// Update accelerations
	//=======================
	for (size_t i = 0; i < system->N; i++)
	{
		system->a[i] = EvaluateForce(i) / system->m;
	}
	
	// K2
	//=====
	for (size_t i = 0; i < system->N; i++)
	{
		system->v[i] = system->v[i] + dtover2 * system->a[i];
	}
}

void CellListCPUSolver::OnSetStatus(System* system)
{
	real dtover2 = dt / (real)2.0;

	CreateStructures();

	// Compute accelerations for first step
	//=======================================
	for (size_t i = 0; i < system->N; i++)
	{
		system->a[i] = EvaluateForce(i) / system->m;
	}
}

void CellListCPUSolver::CreateStructures()
{
	DeleteBuffers();
	CreateBuffers();

	// Cell data population
	//=======================
	for (size_t i = 0; i < system->N; i++)
	{
		A[i].particleId = i;
		A[i].cellId = ComputeCellId(i);
	}

	// Sort A to B
	//==============
	size_t nCells = M*M*M;
	size_t* cellStarts = new size_t[nCells+1];
	size_t* cellOffsets = new size_t[nCells];
	memset(cellStarts, 0, (nCells+1) * sizeof(size_t));
	memset(cellOffsets, 0, nCells * sizeof(size_t));

	for (size_t i = 0; i < system->N; i++)
	{
		cellStarts[A[i].cellId+1]++;
	}

	for (size_t i = 1; i < nCells+1; i++)
	{
		cellStarts[i] += cellStarts[i-1];
	}

	for (size_t i = 0; i < system->N; i++)
	{
		size_t cid = A[i].cellId;
		B[cellStarts[cid] + cellOffsets[cid]] = A[i];
		cellOffsets[cid]++;

	}

	SAFE_DELA(cellOffsets);

	// Sort particle data
	//=====================
	Vector3* newPos = new Vector3[system->N];
	Vector3* newVel = new Vector3[system->N];
	Vector3* newAcc = new Vector3[system->N];
	for (size_t i = 0; i < system->N; i++)
	{
		size_t pid = B[i].particleId;
		newPos[i] = system->r[pid];
		newVel[i] = system->v[pid];
		newAcc[i] = system->a[pid];
	}

	SAFE_DELA(system->r);
	SAFE_DELA(system->v);
	SAFE_DELA(system->a);

	system->r = newPos;
	system->v = newVel;
	system->a = newAcc;

	// Build S
	//==========
	for (size_t i = 0; i < nCells; i++)
	{
		S[i].start = cellStarts[i];
		S[i].end = cellStarts[i+1];
	}

	SAFE_DELA(cellStarts);
}

size_t CellListCPUSolver::ComputeCellId(size_t i)
{
	return (size_t)floor((system->r[i].x + (real)1.0) / (real)2.0 * (real)M) + 
		(size_t)floor((system->r[i].y + (real)1.0) / (real)2.0 * (real)M) * M +
		(size_t)floor((system->r[i].z + (real)1.0) / (real)2.0 * (real)M) * M * M;
}

void CellListCPUSolver::CreateBuffers()
{
	A = new CellParticle[system->N];
	B = new CellParticle[system->N];
	S = new ParticleInterval[M*M*M];
}

void CellListCPUSolver::DeleteBuffers()
{
	SAFE_DELA(A);
	SAFE_DELA(B);
	SAFE_DELA(S);
}

void CellListCPUSolver::Synchronize()
{
	Vector3* nr = new Vector3[system->N];
	Vector3* nv = new Vector3[system->N];
	Vector3* na = new Vector3[system->N];

	for (size_t i = 0; i < system->N; i++)
	{
		size_t pid = B[i].particleId;
		nr[pid] = system->r[i];
		nv[pid] = system->v[i];
		na[pid] = system->a[i];
	}

	SAFE_DELA(system->r);
	SAFE_DELA(system->v);
	SAFE_DELA(system->a);

	system->r = nr;
	system->v = nv;
	system->a = na;
}
