//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include <cuda_runtime.h>

//==============================================================================//
//		Types for CUDA implementations.											//
//==============================================================================//

typedef float3 (*forceFunc)(const float3& ri, const float3& rj);
typedef float (*potentialFunc)(const float3& ri, const float3& rj);

struct cudaFuncs
{
	forceFunc f;
	potentialFunc p;
};

struct cuSystem
{
	size_t N;
	float m;
	float dt;
	float dtover2;
	forceFunc f;
	potentialFunc p;
};

//==============================================================================//
//		float3 operators for CUDA.												//
//																				//
//		The following code may contain useless functions, it is adapted			//
//		from an old personal project.											//
//==============================================================================//

__device__ float3 operator- (const float3& v);
__device__ float3 operator+ (const float3& v1, const float3& v2);
__device__ float3 operator- (const float3& v1, const float3& v2);
__device__ float operator* (const float3& v1, const float3& v2);
__device__ float3 operator+= (float3& v1, const float3& v2);
__device__ float3 operator-= (float3& v1, const float3& v2);
__device__ float3 operator*(const float3& v, const float& t);
__device__ float3 operator*(const float& t, const float3& v);
__device__ float3 operator/ (const float3& v, const float c);