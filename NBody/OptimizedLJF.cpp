//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Commons.h"
#include "OptimizedLJF.h"

#include <cmath>

#ifdef CCUDA
#include "OptimizedLJF.cuh"
#endif

using namespace std;

//==============================================================================//
//		Evaluation of the Lennard-Jones force.									//
//==============================================================================//

OptimizedLJF::OptimizedLJF(real sigma, real epsilon)
{
	s = sigma;
	e = epsilon;
	e24 = -(real)24.0 * e;
	e4 = (real)4.0 * e;
	s2 = s * s;
}

void OptimizedLJF::SetCutOff(real cutoff, const System* system)
{
	Force::SetCutOff(cutoff, system);
	cutoff2 = cutoff * cutoff;
}

Vector3 OptimizedLJF::Evaluate(const Vector3& ri, const Vector3& rj, const System* system)
{
	Vector3 r = rj - ri;
	real nrs = r.NormSquared();
	real nrsInv = (real)1.0 / nrs;

	if (nrs > cutoff2)
		return Vector3();

	real pow6 = powr(s2 * nrsInv, (real)3.0);

	return e24 * ((real)2.0 * pow6 * pow6 - pow6) * r * nrsInv;
}

real OptimizedLJF::EvaluatePotential(const Vector3& ri, const Vector3& rj, const System* system)
{
	real nrs = (ri - rj).NormSquared();
	real pow6 = powr(s2 / nrs, (real)3.0);

	real pot = e4 * (pow6 * pow6 - pow6);

	if (nrs < cutoff2)
		pot -= Urcut;

	return pot;
}