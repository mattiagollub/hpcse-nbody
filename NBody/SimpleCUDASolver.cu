//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "SimpleCUDASolver.h"
#include "cuTypes.cuh"
#include "System.h"

#include <cuda_runtime.h>

void SimpleCUDASolver::CreateBuffers()
{
	devAux = thrust::device_vector<float>(system->N);
	CC(cudaMalloc(&devR, system->N * sizeof(Vector3)));
	CC(cudaMalloc(&devV, system->N * sizeof(Vector3)));
	CC(cudaMalloc(&devA, system->N * sizeof(Vector3)));
	CC(cudaMemcpy(devR, system->r, system->N * sizeof(Vector3), cudaMemcpyHostToDevice));
	CC(cudaMemcpy(devV, system->v, system->N * sizeof(Vector3), cudaMemcpyHostToDevice));
}

//==============================================================================//
//		Force evaluation.														//
//==============================================================================//

__device__ float3 S_Force(int j, float3* r, cuSystem sys)
{
	float3 force = make_float3(0.0f, 0.0f, 0.0f);

	for (size_t i = 0; i < sys.N; i++)
	{
		if (i != j)
		{
			float3 ri = r[i];
			float3 rj = r[j];
			float3 dist =  ri - rj;

			// From the definition of periodic boundary conditions on wikipedia
			if (abs(dist.x) > 1.0f) ri.x -= 2.0f * ((dist.x >= 0.0f) ? 1.0f : -1.0f);
			if (abs(dist.y) > 1.0f) ri.y -= 2.0f * ((dist.y >= 0.0f) ? 1.0f : -1.0f);
			if (abs(dist.z) > 1.0f) ri.z -= 2.0f * ((dist.z >= 0.0f) ? 1.0f : -1.0f);

			force += sys.f(rj, ri);
		}
	}

	return force;
}

//==============================================================================//
//		K2																		//
//==============================================================================//

__global__ void S_UpdateAccelerations(float3* r, float3* v, float3* a, cuSystem sys)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= sys.N)
		return;

	a[tid] = S_Force(tid, r, sys) / sys.m;
}

//==============================================================================//
//		K1																		//
//==============================================================================//

__global__ void S_K1(float3* r, float3* v, cuSystem sys)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= sys.N)
		return;

	r[tid] = r[tid] + sys.dt * v[tid];

	// Periodic boundary conditions
	//===============================
	// Incorrect if we allow movements of more than 2.0 in one step, 
	// but in that case the simulation would be completely unstable anyway.
	// GPUs hate branches but this should not need much time.
	if (r[tid].x < -1.0f) r[tid].x += 2.0f;
	if (r[tid].x >  1.0f) r[tid].x -= 2.0f;
		  				  
	if (r[tid].y < -1.0f) r[tid].y += 2.0f;
	if (r[tid].y >  1.0f) r[tid].y -= 2.0f;
		  				   
	if (r[tid].z < -1.0f) r[tid].z += 2.0f;
	if (r[tid].z >  1.0f) r[tid].z -= 2.0f;
}

//==============================================================================//
//		K2																		//
//==============================================================================//

__global__ void S_K2(float3* r, float3* v, float3* a, cuSystem sys)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= sys.N)
		return;

	v[tid] = v[tid] + sys.dtover2 * a[tid] / sys.m;
}

//==============================================================================//
//		Potential energy evaluation												//
//==============================================================================//

__global__ void S_pot(float3* r, float* pot, cuSystem sys)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= sys.N)
		return;

	for (int i = 0; i < tid; i++)
	{
		float3 ri = r[i];
		float3 rj = r[tid];
		float3 dist =  ri - rj;

		// From the definition of periodic boundary conditions on wikipedia
		if (abs(dist.x) > 1.0f) ri.x -= 2.0f * ((dist.x >= 0.0f) ? 1.0f : -1.0f);
		if (abs(dist.y) > 1.0f) ri.y -= 2.0f * ((dist.y >= 0.0f) ? 1.0f : -1.0f);
		if (abs(dist.z) > 1.0f) ri.z -= 2.0f * ((dist.z >= 0.0f) ? 1.0f : -1.0f);
		
		pot[tid] += sys.p(ri, rj);
	}
}

//==============================================================================//
//		Kernel launch.															//
//==============================================================================//

void SimpleCUDASolver::OnAdvance()
{
	epot = 0.0f;

	cuSystem s;
	s.dt = (float)dt;
	s.dtover2 = (float)(dt / (real)2.0);
	s.m = (float)system->m;
	s.N = system->N;
	cudaFuncs fs = system->force->SetupCUDA();
	s.f = fs.f;
	s.p = fs.p;

	const int nThreads = 256;
	const int nBlocks = system->N / nThreads + (((system->N % nThreads) == 0) ? 0 : 1);

	// K2
	//=====
	S_K2<<<nBlocks, nThreads>>>(devR, devV, devA, s);
	CC(cudaDeviceSynchronize());

	// K1
	//=====
	S_K1<<<nBlocks, nThreads>>>(devR, devV, s);
	CC(cudaDeviceSynchronize());

	// Update accelerations
	//=======================
	S_UpdateAccelerations<<<nBlocks, nThreads>>>(devR, devV, devA, s);
	CC(cudaDeviceSynchronize());

	// K2
	//=====
	S_K2<<<nBlocks, nThreads>>>(devR, devV, devA, s);
	CC(cudaDeviceSynchronize());
}

void SimpleCUDASolver::OnSetStatus(System* system)
{
	cuSystem s;
	s.dt = (float)dt;
	s.dtover2 = (float)(dt / (real)2.0);
	s.m = (float)system->m;
	s.N = system->N;
	cudaFuncs fs = system->force->SetupCUDA();
	s.f = fs.f;
	s.p = fs.p;

	const int nThreads = 256;
	const int nBlocks = system->N / nThreads + (((system->N % nThreads) == 0) ? 0 : 1);
	
	DeleteBuffers();
	CreateBuffers();

	// Compute accelerations for first step
	//=======================================
	S_UpdateAccelerations<<<nBlocks, nThreads>>>(devR, devV, devA, s);
	CC(cudaDeviceSynchronize());
}

real SimpleCUDASolver::OnEvaluatePotential()
{
	cuSystem s;
	s.dt = (float)dt;
	s.dtover2 = (float)(dt / (real)2.0);
	s.m = (float)system->m;
	s.N = system->N;
	cudaFuncs fs = system->force->SetupCUDA();
	s.f = fs.f;
	s.p = fs.p;

	thrust::fill(devAux.begin(), devAux.end(), 0.0f);
	const int nThreads = 256;
	const int nBlocks = system->N / nThreads + (((system->N % nThreads) == 0) ? 0 : 1);

	S_pot<<<nBlocks, nThreads>>>(devR, thrust::raw_pointer_cast(devAux.data()), s);
	CC(cudaDeviceSynchronize());

	return thrust::reduce(devAux.begin(), devAux.end(), 0.0f, thrust::plus<float>());
}