//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "VMDMovie.h"

//==============================================================================//
//		VMDMovie class implementation.											//
//==============================================================================//

VMDMovie::VMDMovie(const char* fileName)
{
	this->fileName = fileName;
	file.open(fileName, ios::out);
	
	if (!file.good())
		LOG("Unable to write to file: %s\n", fileName);

	file.close();
}

VMDMovie::~VMDMovie()
{
	if (file.is_open())
		file.close();
}

void VMDMovie::AddFrame(System* status)
{
	file.open(fileName, ios::out | ios::app);
	
	if (!file.good())
	{
		LOG("Unable to write to file: %s\n", fileName);
		file.close();
		return;
	}

	file << status->N << "\n";
	file << "Atoms" << "\n";

	for (size_t i = 0; i < status->N; i++)
	{
		file << "1 " << status->r[i].x << " " << status->r[i].y << " " << status->r[i].z << "\n";
	}

	file.close();
}
