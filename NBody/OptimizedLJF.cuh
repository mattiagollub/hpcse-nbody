//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include <cuda_runtime.h>

//==============================================================================//
//		Types for CUDA implementation.											//
//==============================================================================//

struct cuOLJF
{
	float s;
	float e;
	float cutoff;
	float cutoff2;
	float e24;
	float e4;
	float s2;
	float Urcut;
};