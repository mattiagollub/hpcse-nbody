//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "CellListCUDASolver.h"
#include "System.h"

#include <string.h>
#include <cmath>

using namespace std;

//==============================================================================//
//		CellListCUDASolver class implementation.								//
//==============================================================================//

CellListCUDASolver::CellListCUDASolver(size_t cells)
{
	h = (real)2.0 / (real)cells;
	M = cells;
	
	devSS = NULL;

	devA = NULL;
	devV = NULL;
	devR = NULL;
	devAux = NULL;
}

CellListCUDASolver::~CellListCUDASolver()
{
	DeleteBuffers();
}

void CellListCUDASolver::DeleteBuffers()
{
	SAFE_DELC(devSS);
	SAFE_DELC(devR);
	SAFE_DELC(devV);
	SAFE_DELC(devA);
	SAFE_DELC(devAux);
}
