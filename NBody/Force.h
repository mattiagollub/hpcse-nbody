//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Vector3.h"

#ifdef CCUDA
#include "cuTypes.cuh"
#endif

class System;

//==============================================================================//
//		Base class for any force between two bodies.							//
//==============================================================================//

class Force
{
public:

	//==========================================================================//
	//	Constructor.															//
	//==========================================================================//

	Force();

	//==========================================================================//
	//	Evaluation.																//
	//==========================================================================//

	virtual void SetCutOff(real cutoff, const System* system);
	virtual Vector3 Evaluate(const Vector3& ri, const Vector3& rj, const System* system) = 0;
	virtual real EvaluatePotential(const Vector3& ri, const Vector3& rj, const System* system) = 0;

	//==========================================================================//
	//	CUDA implementation.													//
	//==========================================================================//

	#ifdef CCUDA
	virtual cudaFuncs SetupCUDA() = 0;
	#endif
	
protected:

	//==========================================================================//
	//	Protected fields.														//
	//==========================================================================//

	real cutoff;
	real Urcut;
};
