//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include <cuda_runtime.h>

//==============================================================================//
//		Types for CUDA implementation.											//
//==============================================================================//

struct cuLJF
{
	float s;
	float e;
	float cutoff;
	float Urcut;
};