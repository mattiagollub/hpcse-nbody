//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#define _USE_MATH_DEFINES

#include "System.h"
#include "Solver.h"
#include "Timer.h"

#include "SimpleCPUSolver.h"

#ifdef CCUDA
#include "SimpleCUDASolver.h"
#include "CellListCUDASolver.h"
#endif

#include "CellListCPUSolver.h"
#include "LennardJonesForce.h"
#include "OptimizedLJF.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>

using namespace std;

//==============================================================================//
//		Normal distribution RNG.												//
//==============================================================================//

real normalRand()
{
#ifdef WIN32
	real u1 = (real)(rand() / (RAND_MAX + 1.0));
	real u2 = (real)(rand() / (RAND_MAX + 1.0));
#else
	real u1 = (real)drand48();
	real u2 = (real)drand48();
#endif
	real ret = (real)sqrtr(-(real)2.0*log(u1))*cos((real)2.0*M_PI*u2);

#ifdef WIN32
	if (!_finite(ret)) ret = (real)0.0;
#else
	if (!isfinite(ret)) ret = (real)0.0;
#endif

	return ret;
}

//==============================================================================//
//		System class implementation.											//
//==============================================================================//

System::System(ArgumentParser& args)
{
	synchronized = true;
	step = 0;

	// Read command line arguments
	const real epsilon = (real)args("-eps").asDouble(1.0);
	const real cutoffMult = (real)args("-cm").asDouble(2.5);
	const real mass = (real)args("-ms").asDouble(1.0);
	const real dt = (real)args("-dt").asDouble(0.001);
	const real energy = (real)args("-et").asDouble(0.0);
	const size_t gridWidth = (size_t)args("-gw").asInt(10);
	const std::string slv = args("-so").asString("clcpu");
	const std::string frc = args("-fo").asString("oljf");
	mt = args("-mt").asBool(true);

	real sigma;
	sigma = powr((real)1.0 / (real)(gridWidth * gridWidth * gridWidth), (real)1.0/(real)3.0);
	real cutoff = sigma * cutoffMult;

	size_t M = (size_t)floor((real)2.0 / cutoff);

	m = mass;
	Etot = energy;

	// Initialize buffers
	N = gridWidth * gridWidth * gridWidth;
	r = new Vector3[N];
	v = new Vector3[N];
	a = new Vector3[N];

	force = NULL;
	solver = NULL;

	if (slv.compare("clcuda") == 0)
		solver = new CellListCUDASolver(M);
	if (slv.compare("clcpu") == 0)	
		solver = new CellListCPUSolver(M);
	if (slv.compare("scuda") == 0)	
		solver = new SimpleCUDASolver();
	if (slv.compare("scpu") == 0)	
		solver = new SimpleCPUSolver();

	if (frc.compare("oljf") == 0)
		force = new OptimizedLJF(sigma, epsilon);
	if (frc.compare("ljf") == 0)
		force = new LennardJonesForce(sigma, epsilon);

	if (solver == NULL)
		LOG("Invalid solver: %s\n", slv);
	if (force == NULL)
		LOG("Invalid force: %s\n", frc);

	force->SetCutOff(cutoff, this);
	solver->SetTimestep(dt);

	// Generate positions and random velocities
	real offset = (real)0.5 / (real)gridWidth;
	for (size_t x = 0; x < gridWidth; x++)
	{
		real px = ((real)x / (real)(gridWidth) + offset - (real)0.5) * (real)2.0;
		size_t bx = x * gridWidth * gridWidth;

		for (size_t y = 0; y < gridWidth; y++)
		{
			real py = ((real)y / (real)(gridWidth) + offset - (real)0.5) * (real)2.0;
			size_t by = gridWidth * y;
			
			for (size_t z = 0; z < gridWidth; z++)
			{
				real pz = ((real)z / (real)(gridWidth) + offset - (real)0.5) * (real)2.0;

				size_t idx = bx + by + z;
				r[idx] = Vector3(px, py, pz);
				v[idx] = Vector3(normalRand(), normalRand(), normalRand());
				a[idx] = Vector3();
			}
		}
	}

	solver->SetStatus(this);

	// Compute energies
	real EkinGen = KineticEnergy();
	real EpotGen = PotentialEnergy();
	real EkinTar = Etot - EpotGen;	

	if (EkinTar <= (real)0.0)
		LOG("The potential energy of the system (%.4f) is too low and forces the kinetic energy to be negative (%.4f), please use a bigger energy for the system.\n", (float)Etot, (float)EkinTar);

	// Rescale velocities to match the system's energy
	real scale = sqrtr(EkinTar / EkinGen);
	for (size_t i = 0; i < N; i++)
	{
		v[i] *= scale;
	}

	solver->SetStatus(this);
}

real System::KineticEnergy()
{
	Synchronize();

	real Ekin = (real)0.0;

	for (size_t i = 0; i < N; i++)
	{
		real vn = v[i].Norm();
		Ekin += m / (real)2.0 * vn * vn;
	}

	return Ekin;
}

real System::PotentialEnergy()
{
	return solver->PotentialEnergy();
}

real System::TotalEnergy()
{
	Synchronize();

	real K = KineticEnergy();
	real P = PotentialEnergy();
	return K + P;
}

Vector3 System::MassCenter()
{
	Synchronize();

	Vector3 center = Vector3();

	for (size_t i = 0; i < N; i++)
	{
		center += r[i] * m;
	}

	return center / (m * (real)N);
}

Vector3 System::AngularMomentum()
{
	Synchronize();

	Vector3 momentum = Vector3();
	for (size_t i = 0; i < N; i++)
	{
		momentum += r[i] ^ (m * v[i]);
	}

	return momentum;
}

Vector3 System::LinearMomentum()
{
	Synchronize();

	Vector3 momentum = Vector3();
	for (size_t i = 0; i < N; i++)
	{
		momentum += m * v[i];
	}

	return momentum;
}

System::~System()
{
	SAFE_DELA(r);
	SAFE_DELA(v);
	SAFE_DELA(a);
}

void System::PlotIntegrals()
{
	Synchronize();

	real energy = TotalEnergy();
	Vector3 am = AngularMomentum();
	Vector3 lm = LinearMomentum();
	Vector3 mc = MassCenter();
	LOG("Ekin: %f | Epot: %f\n", (real)KineticEnergy(), (real)PotentialEnergy());
	LOG("E: %6.4f | AM: %6.4f, %6.4f, %6.4f | LM: %6.4f, %6.4f, %6.4f | MC: %6.4f, %6.4f, %6.4f\n",
		energy, am.x, am.y, am.z, lm.x, lm.y, lm.z, mc.x, mc.y, mc.z);
}

void System::Advance(size_t steps, bool computePotential)
{
	Timer t = Timer();

	for (size_t i = 0; i < steps; i++)
	{
		step++;
		LOG("%u\n", (unsigned int)step);

		if (mt) t.start(); 
		solver->AdvanceOneStep(computePotential);
		if (mt) LOG("Time: %fs\n", t.stop());

		synchronized = false;
	}
}

void System::Synchronize()
{
	if (!synchronized)
	{
		solver->Synchronize();
		synchronized = true;
	}
}