//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "ArgumentParser.h"
#include "SimpleCPUSolver.h"

#ifdef CCUDA
#include "SimpleCUDASolver.h"
#include "CellListCUDASolver.h"
#endif

#include "CellListCPUSolver.h"
#include "LennardJonesForce.h"
#include "OptimizedLJF.h"
#include "VMDMovie.h"

using namespace std;


int main(int argc, char* argv[])
{

	LOG("ETHZ - High Performance Computing for CSE I\n");
	LOG("Project 1 - The N-body problem\n\n");

	LOG("Mattia Gollub (gollubm@student.ethz.ch)\n\n");

	ArgumentParser args(argc, argv);
	System* system = new System(args);
	const std::string output = args("-o").asString("result.xyz");
	const size_t steps = (size_t)args("-st").asInt(100);
	const bool printFI = args("-fi").asBool(true);

	VMDMovie movie(output.c_str());

	movie.AddFrame(system);

	LOG("%5u - ", 0);
	system->PlotIntegrals();

	for (size_t i = 0; i < steps; i++)
	{
		system->Advance(1, printFI);
	
		if (printFI)
			system->PlotIntegrals();
		else
			system->Synchronize();

		movie.AddFrame(system);
	}

	LOG("Simulation complete.\n");

	return 0;
}
