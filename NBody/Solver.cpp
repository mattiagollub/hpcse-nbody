//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Solver.h"
#include "Commons.h"

//==============================================================================//
//		Solver class implementation.											//
//==============================================================================//

Solver::Solver()
{
	system = NULL;
	dt = (real)0.001;
}

void Solver::SetStatus(System* status)
{
	system = status;
	OnSetStatus(status);
	epot = OnEvaluatePotential();
}

void Solver::SetTimestep(real dt)
{
	this->dt = dt;
}

void Solver::AdvanceOneStep(bool computePotential)
{
	OnAdvance();

	if (computePotential)
		epot = OnEvaluatePotential();
}

real Solver::PotentialEnergy()
{
	return epot;
}
