//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Commons.h"
#include "System.h"

#include <fstream>

using namespace std;

//==============================================================================//
//		Result visualization.													//
//==============================================================================//

class VMDMovie
{
public:

	//==========================================================================//
	//	Constructor / Destructor.												//
	//==========================================================================//

	VMDMovie(const char* fileName);
	~VMDMovie();

	//==========================================================================//
	//	Movie managment.														//
	//==========================================================================//

	void AddFrame(System* status);

private:
	ofstream file;
	const char* fileName;
};
