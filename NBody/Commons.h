//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include <math.h>
#include <stdio.h>

#ifdef CCUDA 
#include <cuda_runtime.h>
#endif

using namespace std;

//#define DOUBLE			// Use double for a better (but slower) precision. For test purposes only.

#ifdef DOUBLE
	typedef double real;
	#define ceilr(x) ceil(x)
	#define powr(x, p) pow(x, p)
	#define sqrtr(x) sqrt(x)
#else
	typedef float real;
	#define ceilr(x) ceilf(x)
	#define powr(x, p) powf(x, p)
	#define sqrtr(x) sqrtf(x)
#endif

//==============================================================================//
//		Useful macros.															//
//==============================================================================//

#define SAFE_DEL(ptr) if (ptr != NULL) { delete ptr; ptr = NULL; }
#define SAFE_DELA(ptr) if (ptr != NULL) { delete[] ptr; ptr = NULL; }

#define LOG printf

#ifdef CCUDA 
#define CC(res) if (res != cudaSuccess) { const char* msg = cudaGetErrorString(res); LOG("CUDA error: %s\n", msg);}
#define SAFE_DELC(ptr) 	if (ptr != NULL) { CC(cudaFree(devR)); devR = NULL;	}
#endif

//==============================================================================//
//              Support structures.                                                                                                             //
//==============================================================================//

struct CellParticle
{
        size_t particleId;
        size_t cellId;
};

struct ParticleInterval
{
       size_t start;
       size_t end;
};
