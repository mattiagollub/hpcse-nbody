//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#define THRUST_DEBUG 1

#include "CellListCUDASolver.h"
#include "cuTypes.cuh"
#include "System.h"

#include <cuda_runtime.h>

#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/gather.h>
#include <thrust/iterator/counting_iterator.h>

//==============================================================================//
//		Force evaluation.														//
//==============================================================================//

__global__ void C_pot(float3* r, CellParticle* B, ParticleInterval* S, float* pot, cuSystem sys, size_t M)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= sys.N)
		return;

	// Find adjacent cells
	//======================
	size_t cid = B[tid].cellId;
	size_t x, y, z;
	size_t xm, xp, ym, yp, zm, zp;

	x = cid % M;
	y = (cid % (M*M)) / M;
	z = cid / (M*M);

	if (x == 0) xm = M - 1;	else xm = x - 1;
	if (x == (M - 1)) xp = 0; else xp = x + 1;

	if (y == 0) ym = M - 1; else ym = y - 1;
	if (y == (M - 1)) yp = 0; else yp = y + 1;

	if (z == 0) zm = M - 1;	else zm = z - 1;
	if (z == (M - 1)) zp = 0; else zp = z + 1;

	size_t cx[27] = { xm, xm, xm, xm, xm, xm, xm, xm, xm,  x,  x,  x,  x,  x,  x,  x,  x,  x, xp, xp, xp, xp, xp, xp, xp, xp, xp}; 
	size_t cy[27] = { ym, ym, ym,  y,  y,  y, yp, yp, yp, ym, ym, ym,  y,  y,  y, yp, yp, yp, ym, ym, ym,  y,  y,  y, yp, yp, yp};
	size_t cz[27] = { zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp};
	
	// Compute potentials
	//====================
	for (size_t c = 0; c < 27; c++)
	{
		cid = cx[c] + cy[c] * M + cz[c] * M * M;
		
		for (size_t i = S[cid].start; i < S[cid].end; i++)
		{
			if (tid < i)
			{
				float3 ri = r[i];
				float3 rj = r[tid];
				float3 dist =  ri - rj;

				// From the definition of periodic boundary conditions on wikipedia
				if (abs(dist.x) > 1.0f) ri.x -= 2.0f * ((dist.x >= 0.0f) ? 1.0f : -1.0f);
				if (abs(dist.y) > 1.0f) ri.y -= 2.0f * ((dist.y >= 0.0f) ? 1.0f : -1.0f);
				if (abs(dist.z) > 1.0f) ri.z -= 2.0f * ((dist.z >= 0.0f) ? 1.0f : -1.0f);
	
				pot[tid] += sys.p(rj, ri);
			}
		}
	}
}

//==============================================================================//
//		Force evaluation.														//
//==============================================================================//

__device__ float3 C_Force(int j, float3* r, CellParticle* B, ParticleInterval* S, cuSystem sys, size_t M)
{
	float3 force = make_float3(0.0f, 0.0f, 0.0f);

	// Find adjacent cells
	//======================
	size_t cid = B[j].cellId;
	size_t x, y, z;
	size_t xm, xp, ym, yp, zm, zp;

	x = cid % M;
	y = (cid % (M*M)) / M;
	z = cid / (M*M);

	if (x == 0) xm = M - 1;	else xm = x - 1;
	if (x == (M - 1)) xp = 0; else xp = x + 1;

	if (y == 0) ym = M - 1; else ym = y - 1;
	if (y == (M - 1)) yp = 0; else yp = y + 1;

	if (z == 0) zm = M - 1;	else zm = z - 1;
	if (z == (M - 1)) zp = 0; else zp = z + 1;

	size_t cx[27] = { xm, xm, xm, xm, xm, xm, xm, xm, xm,  x,  x,  x,  x,  x,  x,  x,  x,  x, xp, xp, xp, xp, xp, xp, xp, xp, xp}; 
	size_t cy[27] = { ym, ym, ym,  y,  y,  y, yp, yp, yp, ym, ym, ym,  y,  y,  y, yp, yp, yp, ym, ym, ym,  y,  y,  y, yp, yp, yp};
	size_t cz[27] = { zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp, zm,  z, zp};
	
	// Compute forces
	//=================
	for (size_t c = 0; c < 27; c++)
	{
		cid = cx[c] + cy[c] * M + cz[c] * M * M;
		
		for (size_t i = S[cid].start; i < S[cid].end; i++)
		{
			if (i != j)
			{
				float3 ri = r[i];
				float3 rj = r[j];
				float3 dist =  ri - rj;

				// From the definition of periodic boundary conditions on wikipedia
				if (abs(dist.x) > 1.0f) ri.x -= 2.0f * ((dist.x >= 0.0f) ? 1.0f : -1.0f);
				if (abs(dist.y) > 1.0f) ri.y -= 2.0f * ((dist.y >= 0.0f) ? 1.0f : -1.0f);
				if (abs(dist.z) > 1.0f) ri.z -= 2.0f * ((dist.z >= 0.0f) ? 1.0f : -1.0f);
	
				force += sys.f(rj, ri);
			}
		}
	}

	return force;
}

//==============================================================================//
//		K2																		//
//==============================================================================//

__global__ void C_UpdateAccelerations(float3* r, float3* v, float3* a, CellParticle* B, ParticleInterval* S, cuSystem sys, size_t M)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= sys.N)
		return;

	a[tid] = C_Force(tid, r, B, S, sys, M) / sys.m;
}

//==============================================================================//
//		K1																		//
//==============================================================================//

__global__ void C_K1(float3* r, float3* v, cuSystem sys)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= sys.N)
		return;

	r[tid] = r[tid] + sys.dt * v[tid];

	// Periodic boundary conditions
	//===============================
	// Incorrect if we allow movements of more than 2.0 in one step, 
	// but in that case the simulation would be completely unstable anyway.
	// GPUs hate branches but this should not need much time.
	if (r[tid].x < -1.0f) r[tid].x += 2.0f;
	if (r[tid].x >  1.0f) r[tid].x -= 2.0f;
		  				  
	if (r[tid].y < -1.0f) r[tid].y += 2.0f;
	if (r[tid].y >  1.0f) r[tid].y -= 2.0f;
		  				   
	if (r[tid].z < -1.0f) r[tid].z += 2.0f;
	if (r[tid].z >  1.0f) r[tid].z -= 2.0f;
}

//==============================================================================//
//		K2																		//
//==============================================================================//

__global__ void C_K2(float3* r, float3* v, float3* a, cuSystem sys)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= sys.N)
		return;

	v[tid] = v[tid] + sys.dtover2 * a[tid] / sys.m;
}

//==============================================================================//
//		Kernel launch.															//
//==============================================================================//

void CellListCUDASolver::OnAdvance()
{
	cuSystem s;
	s.dt = (float)dt;
	s.dtover2 = (float)(dt / (real)2.0);
	s.m = (float)system->m;
	s.N = system->N;
	cudaFuncs fs = system->force->SetupCUDA();
	s.f = fs.f;
	s.p = fs.p;

	const int nThreads = 256;
	const int nBlocks = system->N / nThreads + (((system->N % nThreads) == 0) ? 0 : 1);

	// K2
	//=====
	C_K2<<<nBlocks, nThreads>>>(devR, devV, devA, s);
	CC(cudaDeviceSynchronize());

	// K1
	//=====
	C_K1<<<nBlocks, nThreads>>>(devR, devV, s);
	CC(cudaDeviceSynchronize());

	// Update accelerations
	//=======================
	CreateStructures();
	CC(cudaDeviceSynchronize());
	C_UpdateAccelerations<<<nBlocks, nThreads>>>(devR, devV, devA, thrust::raw_pointer_cast(devSB.data()), devSS, s, M);
	CC(cudaDeviceSynchronize());

	// K2
	//=====
	C_K2<<<nBlocks, nThreads>>>(devR, devV, devA, s);
	CC(cudaDeviceSynchronize());
}

void CellListCUDASolver::OnSetStatus(System* system)
{
	cuSystem s;
	s.dt = (float)dt;
	s.dtover2 = (float)(dt / (real)2.0);
	s.m = (float)system->m;
	s.N = system->N;
	cudaFuncs fs = system->force->SetupCUDA();
	s.f = fs.f;
	s.p = fs.p;

	const int nThreads = 256;
	const int nBlocks = system->N / nThreads + (((system->N % nThreads) == 0) ? 0 : 1);
	
	DeleteBuffers();
	CreateBuffers();

	// Compute accelerations for first step
	//=======================================
	CreateStructures();
	CC(cudaDeviceSynchronize());
	C_UpdateAccelerations<<<nBlocks, nThreads>>>(devR, devV, devA, thrust::raw_pointer_cast(devSB.data()), devSS, s, M);
	CC(cudaDeviceSynchronize());
}

//==============================================================================//
//		Create support structures												//
//==============================================================================//

struct CellComp {
  
	__host__ __device__ bool operator()(const CellParticle& p1, const CellParticle& p2)
	{
		return p1.cellId < p2.cellId;
	}
};

__device__ size_t ComputeCellId(float3 r, size_t M)
{
	return (size_t)floor((r.x + 1.0f) / 2.0f * (float)M) + 
		   (size_t)floor((r.y + 1.0f) / 2.0f * (float)M) * M +
		   (size_t)floor((r.z + 1.0f) / 2.0f * (float)M) * M * M;
}

__global__ void PopulateA(CellParticle* A, float3* r, size_t N, size_t M)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= N)
		return;

	A[tid].particleId = tid;
	A[tid].cellId = ComputeCellId(r[tid], M);
}

__global__ void SortParticleData(float3* src, float3* dest, CellParticle* keys, size_t N)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= N)
		return;

	CellParticle p = keys[tid];
	dest[tid] = src[p.particleId];
}

__global__ void SortBack(float3* src, float3* dest, CellParticle* keys, size_t N)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= N)
		return;

	size_t pid = keys[tid].particleId;
	dest[pid] = src[tid];
}

__global__ void BuildIntervals(CellParticle* cells, ParticleInterval* intervals, size_t N, size_t M3)
{
	int tid = threadIdx.x + blockIdx.x * blockDim.x;
	if (tid >= N)
		return;

	if (tid == N-1)
	{
		intervals[cells[N-1].cellId].end = N;

		// Check for empty cells
		for (int i = cells[N-1].cellId + 1; i < M3; i++)
		{
			intervals[i].start = N;
			intervals[i].end = N;
		}
	}
	else
	{
		int first = cells[tid].cellId;
		int last = cells[tid+1].cellId;

		if (first == last)
		{
			// We are in the middle of an interval: do nothing
		}
		else
		{
			intervals[first].end = tid + 1;
			intervals[last].start = tid + 1;

			// Check for empty cells
			for (int i = first + 1; i < last - 1; i++)
			{
				intervals[i].start = tid + 1;
				intervals[i].end = tid + 1;
			}
		}

		if (tid == 0)
		{
			intervals[0].start = 0;
		}
	}
}


void CellListCUDASolver::CreateStructures()
{
	const int nThreads = 256;
	const int nBlocks = system->N / nThreads + (((system->N % nThreads) == 0) ? 0 : 1);

	// Cell data population
	//=======================
	PopulateA<<<nBlocks, nThreads>>>(thrust::raw_pointer_cast(devSB.data()), devR, system->N, M);

	// Sort data
	//============
	thrust::sort(devSB.begin(), devSB.end(), CellComp());

	// Sort particle data to improve locality
	//=========================================
	float3* tmp;
	SortParticleData<<<nBlocks, nThreads>>>(devR, devAux, thrust::raw_pointer_cast(devSB.data()), system->N);
	CC(cudaDeviceSynchronize());

	tmp = devAux;
	devAux = devR;
	devR = tmp;

	SortParticleData<<<nBlocks, nThreads>>>(devV, devAux, thrust::raw_pointer_cast(devSB.data()), system->N);
	CC(cudaDeviceSynchronize());

	tmp = devAux;
	devAux = devV;
	devV = tmp;

	SortParticleData<<<nBlocks, nThreads>>>(devA, devAux, thrust::raw_pointer_cast(devSB.data()), system->N);
	CC(cudaDeviceSynchronize());

	tmp = devAux;
	devAux = devA;
	devA = tmp;

	// Create S
	//===========
	BuildIntervals<<<nBlocks, nThreads>>>(thrust::raw_pointer_cast(devSB.data()), devSS, system->N, M*M*M);
	CC(cudaDeviceSynchronize());
}

void CellListCUDASolver::CreateBuffers()
{
	// Particle data
	//================
	CC(cudaMalloc(&devR, system->N * sizeof(Vector3)));
	CC(cudaMalloc(&devV, system->N * sizeof(Vector3)));
	CC(cudaMalloc(&devA, system->N * sizeof(Vector3)));
	CC(cudaMalloc(&devAux, system->N * sizeof(Vector3)));
	CC(cudaMemcpy(devR, system->r, system->N * sizeof(Vector3), cudaMemcpyHostToDevice));
	CC(cudaMemcpy(devV, system->v, system->N * sizeof(Vector3), cudaMemcpyHostToDevice));

	// Support structures
	//=====================
	CC(cudaMalloc(&devSS, sizeof(ParticleInterval) * M * M * M));
	devSB = thrust::device_vector<CellParticle>(system->N);
	devPot = thrust::device_vector<float>(system->N);
}

//==============================================================================//
//		Copy back																//
//==============================================================================//

void CellListCUDASolver::Synchronize()
{
	const int nThreads = 256;
	const int nBlocks = system->N / nThreads + (((system->N % nThreads) == 0) ? 0 : 1);

	SortBack<<<nBlocks, nThreads>>>(devR, devAux, thrust::raw_pointer_cast(devSB.data()), system->N);
	CC(cudaDeviceSynchronize());
	CC(cudaMemcpy(system->r, devAux, system->N * sizeof(Vector3), cudaMemcpyDeviceToHost));

	SortBack<<<nBlocks, nThreads>>>(devV, devAux, thrust::raw_pointer_cast(devSB.data()), system->N);
	CC(cudaDeviceSynchronize());
	CC(cudaMemcpy(system->v, devAux, system->N * sizeof(Vector3), cudaMemcpyDeviceToHost));

	SortBack<<<nBlocks, nThreads>>>(devA, devAux, thrust::raw_pointer_cast(devSB.data()), system->N);
	CC(cudaDeviceSynchronize());
	CC(cudaMemcpy(system->a, devAux, system->N * sizeof(Vector3), cudaMemcpyDeviceToHost));
}

//==============================================================================//
//		Potential energy														//
//==============================================================================//

real CellListCUDASolver::OnEvaluatePotential()
{
	cuSystem s;
	s.dt = (float)dt;
	s.dtover2 = (float)(dt / (real)2.0);
	s.m = (float)system->m;
	s.N = system->N;
	cudaFuncs fs = system->force->SetupCUDA();
	s.f = fs.f;
	s.p = fs.p;
	
	thrust::fill(devPot.begin(), devPot.end(), 0.0f);
	const int nThreads = 256;
	const int nBlocks = system->N / nThreads + (((system->N % nThreads) == 0) ? 0 : 1);
	
	C_pot<<<nBlocks, nThreads>>>(devR, thrust::raw_pointer_cast(devSB.data()), devSS, thrust::raw_pointer_cast(devPot.data()), s, M);
	CC(cudaDeviceSynchronize());
	
	return (real)thrust::reduce(devPot.begin(), devPot.end(), 0.0f, thrust::plus<float>());
}