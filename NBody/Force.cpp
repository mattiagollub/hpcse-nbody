//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include <cmath>
#include <cfloat>

#include "Force.h"
#include "System.h"

using namespace std;

//==============================================================================//
//		Force class implementation.												//
//==============================================================================//

void Force::SetCutOff(real cutoff, const System* system)
{
	this->cutoff = cutoff;
	this->Urcut = EvaluatePotential(Vector3(), Vector3((real)0.0, (real)0.0, cutoff), system);
}

Force::Force()
{
	cutoff = (real)FLT_MAX;
	this->Urcut = (real)0.0;
}