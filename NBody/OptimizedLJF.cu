//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#include "OptimizedLJF.h"
#include "OptimizedLJF.cuh"
#include "cuTypes.cuh"

#include <cuda_runtime.h>

//==============================================================================//
//		CUDA implementation.													//
//==============================================================================//

__device__ __constant__ cuOLJF OLJFdata;

__device__ float3 OLJF(const float3& ri, const float3& rj)
{
	float3 r = rj - ri;
	float nrs = r * r;
	float nrsinv = 1.0f /  nrs;

	if (nrs > OLJFdata.cutoff2)
		return make_float3(0.0f, 0.0f, 0.0f);

	float pow6 = pow(OLJFdata.s2 * nrsinv, 3.0f);

	return OLJFdata.e24 * (2.0f * pow6 * pow6 - pow6) * r * nrsinv;
}

__device__ float OLJP(const float3& ri, const float3& rj)
{
	float3 r = ri - rj;
	float nrs = r * r;
	float pow6 = pow(OLJFdata.s2 / nrs, 3.0f);

	float pot = OLJFdata.e4 * (pow6 * pow6 - pow6);

	if (nrs < OLJFdata.cutoff2)
		pot -= OLJFdata.Urcut;

	return pot;
}

__device__ __constant__ forceFunc OLJFfunc = &OLJF;
__device__ __constant__ potentialFunc OLJPfunc = &OLJP;

cudaFuncs OptimizedLJF::SetupCUDA()
{
	cuOLJF cuf;
	cuf.cutoff = (float)cutoff;
	cuf.e = (float)e;
	cuf.s = (float)s;
	cuf.s2 = cuf.s * cuf.s;
	cuf.cutoff2 = cuf.cutoff * cuf.cutoff;
	cuf.e24 = -24.0f * cuf.e;
	cuf.e4 = 4.0f * cuf.e;
	cuf.Urcut = Urcut;

	cudaFuncs f;

	CC(cudaMemcpyToSymbol(OLJFdata, &cuf, sizeof(cuOLJF)));
	CC(cudaMemcpyFromSymbol(&f.f, OLJFfunc, sizeof(forceFunc)));
	CC(cudaMemcpyFromSymbol(&f.p, OLJPfunc, sizeof(potentialFunc)));

	return f;
}
