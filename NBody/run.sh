#!/bin/bash
TEST_OPT=-cm 2.5 -dt 0.001 -et 0.0 -cs 0.25 -gw 10 -st 50

bsub -n 1 -W 00:02 -U gpuclass_tue nbody $TEST_OPT -so scpu
bsub -n 1 -W 00:02 -U gpuclass_tue nbody $TEST_OPT -so clcpu
bsub -n 1 -W 00:02 -R gpu -R 'cuda5 && cudaversion==5000' -U gpuclass_tue nbody $TEST_OPT -so scuda
bsub -n 1 -W 00:02 -R gpu -R 'cuda5 && cudaversion==5000' -U gpuclass_tue nbody $TEST_OPT -so clcuda
