//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "Commons.h"
#include "Vector3.h"
#include "Force.h"
#include "ArgumentParser.h"
#include "Solver.h"

#include <string>

//==============================================================================//
//		Status of the a NBody system at a time t.								//
//==============================================================================//

class System
{
public:

	//==========================================================================//
	//	Public fields.															//
	//==========================================================================//

	size_t N;
	real m;
	real Etot;
	Vector3* r;
	Vector3* v;
	Vector3* a;
	size_t step;
	Force* force;
	Solver* solver;
	bool synchronized;

	//==========================================================================//
	//	Constructor / Destructor.												//
	//==========================================================================//

	System(ArgumentParser& args);
	~System();

	//==========================================================================//
	//	Energies.																//
	//==========================================================================//

	real KineticEnergy();
	real PotentialEnergy();

	//==========================================================================//
	//	First integrals.														//
	//==========================================================================//

	real TotalEnergy();
	Vector3 MassCenter();
	Vector3 AngularMomentum();
	Vector3 LinearMomentum();
	void PlotIntegrals();

	//==========================================================================//
	//	Simulation.																//
	//==========================================================================//
	
	void Advance(size_t steps = 1, bool computePotential = true);
	void Synchronize();

private:
	bool mt;
};
