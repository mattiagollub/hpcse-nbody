//==============================================================================//
//																				//
//		ETHZ - High Performance Computing for CSE I								//
//		Project 1 - The N-body problem											//
//																				//
//		Cpyright � 2013 Mattia Gollub (gollubm@student.ethz.ch)					//
//																				//
//==============================================================================//

#pragma once

#include "SimpleCUDASolver.h"
#include "System.h"

#include <cuda_runtime.h>

//==============================================================================//
//		SimpleCUDASolver class implementation.									//
//==============================================================================//

SimpleCUDASolver::SimpleCUDASolver()
{
	devR = NULL;
	devV = NULL;
	devA = NULL;
}

void SimpleCUDASolver::DeleteBuffers()
{ 
	SAFE_DELC(devR);
	SAFE_DELC(devV);
	SAFE_DELC(devA);
}

void SimpleCUDASolver::Synchronize()
{
	CC(cudaMemcpy(system->r, devR, system->N * sizeof(Vector3), cudaMemcpyDeviceToHost));
	CC(cudaMemcpy(system->v, devV, system->N * sizeof(Vector3), cudaMemcpyDeviceToHost));
}